package com.example.q26;

import android.annotation.SuppressLint;
import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;

import androidx.core.view.WindowCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.q26.databinding.ActivityMainBinding;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Button btn;
    EditText etext1, etext2, etext3;
    TextView text2;


    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn = findViewById(R.id.btn);
        etext1 = findViewById(R.id.etext1);
        etext2 = findViewById(R.id.etext2);
        etext3 = findViewById(R.id.etext3);
        text2 = findViewById(R.id.text2);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int num1 = Integer.parseInt(etext1.getText().toString());
                int num2 = Integer.parseInt(etext2.getText().toString());
                int num3 = Integer.parseInt(etext3.getText().toString());

                int temp = num1>num2?num1:num2;
                int largest = num3>temp?num3:temp;

                text2.setText("The largest number is: "+largest);

            }
        });


    }
}